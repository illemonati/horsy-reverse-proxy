package main

import (
	"log"

	"github.com/alecthomas/kingpin/v2"
)

var (
	configPath = kingpin.Flag("config", "Path to configuration file.").Short('c').Default("./configs/sample-config.json").String()
)

func main() {
	kingpin.Parse()
	config, err := ConfigFromFile(*configPath)
	if err != nil {
		log.Panicf("Unable to read config: %v\n", err)
	}
	StartServers(config)
}

func StartServers(config Config) {
	// log.Println(config)
	pool := NewServePool()
	for _, serverConfig := range config.Servers {
		server, err := NewServer(serverConfig)
		if err != nil {
			log.Fatalf("Unable to create server: %v", err)
		}
		log.Printf("Adding %v to pool.", server)
		err = pool.AddServer(server)
		if err != nil {
			log.Fatalf("Unable to add server: %v", err)
		}
	}
	pool.Serve()
}
