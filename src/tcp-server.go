package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"sync"
)

type TCPServer struct {
	config      ServerConfig
	listener    net.Listener
	infoLogger  *log.Logger
	errorLogger *log.Logger
}

func NewTCPServer(config ServerConfig) *TCPServer {
	server := &TCPServer{
		config:      config,
		listener:    nil,
		infoLogger:  nil,
		errorLogger: nil,
	}
	server.infoLogger = log.New(os.Stdout, fmt.Sprintf("[Info] from %v:", server), log.Ldate|log.Ltime|log.Lmsgprefix)
	server.errorLogger = log.New(os.Stdout, fmt.Sprintf("[Error] from %v:", server), log.Ldate|log.Ltime|log.Lmsgprefix)
	return server
}

func (s *TCPServer) SetListener(listener net.Listener) {
	s.listener = listener
}

func (s *TCPServer) Serve(wg *sync.WaitGroup) {
	defer wg.Done()
	for {
		lconn, err := s.listener.Accept()
		if err != nil {
			s.errorLogger.Println(err)
			continue
		}

		go s.HandleClient(lconn)
	}
}

func (s *TCPServer) HandleClient(lconn net.Conn) {
	s.infoLogger.Printf("Accepted listen conn from %s", lconn.RemoteAddr())
	defer lconn.Close()

	fconn, err := net.Dial("tcp", s.ForwardAddress())
	if err != nil {
		s.errorLogger.Printf("Unable to connect to forward (%s): %v", s.ForwardAddress(), err)
	}
	defer fconn.Close()

	buffer := make([]byte, 1024)
	for {
		_, err := lconn.Read(buffer)
		if err != nil {
			if err == io.EOF {
				break
			} else {
				s.errorLogger.Printf("Unable to read from listen (%s): %v", lconn.RemoteAddr(), err)
				return
			}
		}

		_, err = fconn.Write(buffer)
		buffer = make([]byte, 1024)
		if err != nil {
			s.errorLogger.Printf("Unable to write to forward (%s): %v", fconn.RemoteAddr(), err)
			return
		}
	}
}

func (s *TCPServer) ForwardAddress() string {
	return fmt.Sprintf("%s:%d", s.ForwardHost(), s.ForwardPort())
}

func (s *TCPServer) ListenAddress() string {
	return fmt.Sprintf(":%d", s.ListenPort())
}

func (*TCPServer) Type() string {
	return "TCP"
}

func (s *TCPServer) Name() string {
	return s.config.Name
}

func (s *TCPServer) ForwardPort() uint16 {
	return s.config.ForwardPort
}

func (s *TCPServer) ListenPort() uint16 {
	return s.config.ListenPort
}

func (s *TCPServer) ForwardHost() string {
	fhost := s.config.ForwardHost
	if len(fhost) < 1 {
		fhost = "[::1]"
	}
	return fhost
}

func (s *TCPServer) String() string {
	return ServerString(s)
}
