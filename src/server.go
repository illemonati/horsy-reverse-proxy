package main

import (
	"errors"
	"fmt"
	"net"
	"strings"
)

type Server interface {
	Name() string
	Type() string
	ListenPort() uint16
	ForwardPort() uint16
	ForwardHost() string
	ForwardAddress() string
	String() string
	SetListener(net.Listener)
	ListenAddress() string
}

func NewServer(config ServerConfig) (server Server, err error) {
	switch strings.ToUpper(config.Type) {
	case "TCP":
		server = NewTCPServer(config)
	case "HTTP":
		server = NewHTTPServer(config)
	default:
		err = errors.New("invalid server type")
	}
	return
}

func ServerString(s Server) string {
	return fmt.Sprintf("<%s>[%s](%s->%s)", s.Name(), s.Type(), s.ListenAddress(), s.ForwardAddress())
}
