package main

import (
	"encoding/json"
	"io"
	"os"
)

type Config struct {
	Servers []ServerConfig `json:"servers"`
}

type ServerConfig struct {
	Name        string `json:"name"`
	Type        string `json:"type"`
	ListenPort  uint16 `json:"listen-port"`
	ForwardPort uint16 `json:"forward-port"`
	ForwardHost string `json:"forward-host"`
	HTTPHost    string `json:"http-host"`
}

func ConfigFromFile(path string) (config Config, err error) {
	file, err := os.Open(path)
	if err != nil {
		return
	}
	defer file.Close()

	fbytes, err := io.ReadAll(file)
	if err != nil {
		return
	}

	err = json.Unmarshal(fbytes, &config)

	return
}
