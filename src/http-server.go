package main

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"os"
)

type HTTPServer struct {
	config       ServerConfig
	listener     net.Listener
	infoLogger   *log.Logger
	errorLogger  *log.Logger
	HttpServeMux *http.ServeMux
}

func NewHTTPServer(config ServerConfig) *HTTPServer {
	server := &HTTPServer{
		config:       config,
		listener:     nil,
		infoLogger:   nil,
		errorLogger:  nil,
		HttpServeMux: http.NewServeMux(),
	}
	server.infoLogger = log.New(os.Stdout, fmt.Sprintf("[Info] from %v:", server), log.Ldate|log.Ltime|log.Lmsgprefix)
	server.errorLogger = log.New(os.Stdout, fmt.Sprintf("[Error] from %v:", server), log.Ldate|log.Ltime|log.Lmsgprefix)

	server.setUpHTTPHandler()

	return server
}

func (s *HTTPServer) setUpHTTPHandler() {
	s.HttpServeMux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		s.infoLogger.Printf("Handling [%s] %s", r.Method, r.RequestURI)
		body, err := io.ReadAll(r.Body)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			s.errorLogger.Printf("Unable to read body: %v", err)
			return
		}

		r.Body = io.NopCloser(bytes.NewReader(body))
		forwardURL := fmt.Sprintf("%s%s", s.ForwardAddress(), r.RequestURI)
		forwardReq, err := http.NewRequest(r.Method, forwardURL, bytes.NewReader(body))

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			s.errorLogger.Printf("Unable to construct forward request: %v", err)
			return
		}

		forwardReq.Header = make(http.Header)
		for h, val := range r.Header {
			forwardReq.Header[h] = val
		}
		ogForwardedFor, ogHasForwardedFor := r.Header["X-Forwarded-For"]
		forwardedFor := []string{}
		if ogHasForwardedFor {
			forwardedFor = ogForwardedFor
		}
		forwardedFor = append(forwardedFor, r.RemoteAddr)
		forwardReq.Header["X-Forwarded-For"] = forwardedFor

		resp, err := http.DefaultClient.Do(forwardReq)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadGateway)
			s.errorLogger.Printf("Unable to perform forward request: %v", err)
			return
		}

		for h, val := range resp.Header {
			w.Header()[h] = val
		}
		w.WriteHeader(resp.StatusCode)

		if resp.Body != nil {
			defer resp.Body.Close()
			io.Copy(w, resp.Body)
		}
	})
}

func (s *HTTPServer) SetListener(listener net.Listener) {
	s.listener = listener
}

func (s *HTTPServer) ForwardAddress() string {
	return fmt.Sprintf("%s:%d", s.ForwardHost(), s.ForwardPort())
}

func (s *HTTPServer) ListenAddress() string {
	return fmt.Sprintf("%s:%d", s.HTTPHost(), s.ListenPort())
}

func (*HTTPServer) Type() string {
	return "HTTP"
}

func (s *HTTPServer) Name() string {
	return s.config.Name
}

func (s *HTTPServer) ForwardPort() uint16 {
	return s.config.ForwardPort
}

func (s *HTTPServer) ListenPort() uint16 {
	return s.config.ListenPort
}

func (s *HTTPServer) ForwardHost() string {
	fhost := s.config.ForwardHost
	if len(fhost) < 1 {
		fhost = "[::1]"
	}
	return fhost
}

func (s *HTTPServer) HTTPHost() string {
	return s.config.HTTPHost
}

func (s *HTTPServer) String() string {
	return ServerString(s)
}
