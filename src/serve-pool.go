package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"sync"
)

type ServePool struct {
	ServersByListenPort               map[uint16]([]Server)
	ListenersByListenPort             map[uint16](net.Listener)
	HTTPServersByHTTPHostByListenPort map[uint16](map[string]*HTTPServer)
}

func NewServePool() *ServePool {
	return &ServePool{
		ServersByListenPort:               make(map[uint16][]Server),
		ListenersByListenPort:             make(map[uint16]net.Listener),
		HTTPServersByHTTPHostByListenPort: make(map[uint16]map[string]*HTTPServer),
	}
}

func (p *ServePool) AddServer(server Server) error {
	lp := server.ListenPort()
	serverType := server.Type()
	serverList, ok := p.ServersByListenPort[lp]
	if !ok {
		serverList = make([]Server, 0)
	}

	switch serverType {
	case "TCP":
		serverList = append(serverList, server)
		// tcp requires exclusive listen port
		if len(serverList) > 1 {
			return fmt.Errorf("TCP Server requires exclusive listen port, port %d is being shared by %v", lp, serverList)

		}
		listener, err := net.Listen("tcp", fmt.Sprintf("[::0]:%d", lp))
		if err != nil {
			return err
		}
		p.ListenersByListenPort[lp] = listener
	case "HTTP":
		server, ok := server.(*HTTPServer)
		if !ok {
			return fmt.Errorf("invalid HTTP Server: %v", server)
		}
		serverList = append(serverList, server)
		// HTTP allows for multiple listeners on same port if they have different hosts
		httpServersByHTTPHost, ok := p.HTTPServersByHTTPHostByListenPort[lp]
		if !ok {
			httpServersByHTTPHost = make(map[string]*HTTPServer)
		}
		httpHost := server.HTTPHost()
		if _, exists := httpServersByHTTPHost[httpHost]; exists {
			// cannot have same httphost listen on same port
			return fmt.Errorf("HTTP Server requires exclusive HTTPHost when sharing the same port, port %d is being shared by %v", lp, serverList)
		}
		httpServersByHTTPHost[httpHost] = server
		httpServersByHTTPHost[server.ListenAddress()] = server
		if _, listenerExists := p.ListenersByListenPort[lp]; !listenerExists {
			listener, err := net.Listen("tcp", fmt.Sprintf("[::0]:%d", lp))
			if err != nil {
				return err
			}
			p.ListenersByListenPort[lp] = listener
		}
		p.HTTPServersByHTTPHostByListenPort[lp] = httpServersByHTTPHost
	}

	p.ServersByListenPort[lp] = serverList
	return nil
}

func (p *ServePool) Serve() {
	wg := new(sync.WaitGroup)
	for port, serverList := range p.ServersByListenPort {
		log.Printf("------------------------------------------")
		log.Printf("Starting servers for port %d\n", port)
		listener, ok := p.ListenersByListenPort[port]
		if !ok {
			log.Panicf("Listener not found for port %p\n", &port)
		}
	serverFor:
		for _, server := range serverList {
			switch s := server.(type) {
			case *TCPServer:
				server.SetListener(listener)
				log.Printf("Starting %v", server)
				wg.Add(1)
				go s.Serve(wg)
			case *HTTPServer:
				wg.Add(1)
				go p.handleHTTPServers(port)
				log.Printf("Starting http handler for %v", serverList)
				break serverFor
			}
		}
	}
	wg.Wait()
}

func (p *ServePool) handleHTTPServers(port uint16) {
	serveMux := http.NewServeMux()
	listener, ok := p.ListenersByListenPort[port]
	if !ok {
		log.Panicf("Listener not found for port %p\n", &port)
	}
	httpServersByHTTPHost, ok := p.HTTPServersByHTTPHostByListenPort[port]
	if !ok {
		log.Panicf("HTTP server table not found for port %p\n", &port)
	}
	defer http.Serve(listener, serveMux)

	defaultServer, defaultExists := httpServersByHTTPHost[""]

	serveMux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		hostServer, hostServerExists := httpServersByHTTPHost[r.Host]
		if hostServerExists {
			handler, _ := hostServer.HttpServeMux.Handler(r)
			handler.ServeHTTP(w, r)
		} else {
			if defaultExists {
				handler, _ := defaultServer.HttpServeMux.Handler(r)
				handler.ServeHTTP(w, r)
			} else {
				w.WriteHeader(404)
				fmt.Fprintf(w, "Page not found\n")
			}
		}
	})
}
